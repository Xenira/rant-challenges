fib(0, 1);
fib(0, 1, 20, 3)

function fib(a, b, count = 10, fizz) {
    next = a + b;
    if (fizz && next % fizz === 0) {
        console.log('fizz');
    } else {
        console.log(next)
    }
    if (count > 0) {
        fib(b, next, count - 1, fizz);
    }
}